package com.app.hotels.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.hotels.entities.UserProfile;
import com.app.hotels.exception.UnAuthorizedException;
import com.app.hotels.repository.UserRepository;
import com.app.hotels.response.ResponseWrapper;

@Service
public class UserService {
	
	@Autowired
	private UserRepository userRepository;
	
	public ResponseWrapper<Void> registerUser(UserProfile user){
		
		userRepository.save(user);
		
		ResponseWrapper<Void> wrapper = new ResponseWrapper<Void>();
		wrapper.setMessage("Successfully registered.");
		wrapper.setSuccess(true);
		return wrapper;
	}

	public String validateUser(String username, String password) {
		if(username == null || password == null) {
			throw new UnAuthorizedException("Invalid Credentials");
		}
		Optional<UserProfile> user = userRepository.findByEmailAndPassword(username, password);
		if(!user.isPresent()) {
			throw new UnAuthorizedException("Invalid Credentials");
		}
			
		return user.get().getUserRole();
	}
}
