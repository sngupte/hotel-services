package com.app.hotels.services;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import com.app.hotels.response.ResponseWrapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.app.hotels.entities.Hotels;
import com.app.hotels.exception.BadRequestException;
import com.app.hotels.exception.DataNotFoundException;
import com.app.hotels.repository.HotelRepository;

@Service
public class HotelsServices {

	@Autowired
	private HotelRepository hotelRepository;

	public ResponseWrapper<Void> addHotel(List<Hotels> hotels) {

		if (hotels.isEmpty()) {
			throw new BadRequestException("Empty request.");
		}
		

		hotelRepository.saveAll(hotels);

		ResponseWrapper<Void> wrapper = new ResponseWrapper<>();
		wrapper.setSuccess(true);
		wrapper.setMessage("Added successfully");
		return wrapper;
	}

	public ResponseWrapper<List<Hotels>> showHotels() {

		List<Hotels> hotelList = hotelRepository.findAll();

		ResponseWrapper<List<Hotels>> wrapper = new ResponseWrapper<>();
		wrapper.setSuccess(true);
		wrapper.setData(hotelList);
		return wrapper;
	}

	public ResponseWrapper<Void> deleteHotel(int id) {

		if (!hotelRepository.existsById(id)) {
			throw new DataNotFoundException("Hotel Not Found");
		}

		hotelRepository.deleteById(id);

		ResponseWrapper<Void> wrapper = new ResponseWrapper<>();

		wrapper.setMessage("Data deleted.");
		wrapper.setSuccess(true);
		return wrapper;

	}

	public ResponseWrapper<Void> updateDetails(int id, Hotels hotelRequest) {
		ResponseWrapper<Void> wrapper = new ResponseWrapper<>();

		Optional<Hotels> hotelOpt = hotelRepository.findById(id);
		if (!hotelOpt.isPresent()) {
			throw new DataNotFoundException("Hotel Not Found");
		}

		Hotels hotel = hotelOpt.get();

		if (hotelRequest.getAddress() != null) {
			hotel.setAddress(hotelRequest.getAddress());
		}
		if (hotelRequest.getName() != null) {
			hotel.setName(hotelRequest.getName());
		}
		if (hotelRequest.getPhone() != null) {
			hotel.setPhone(hotelRequest.getPhone());
		}
		if (hotelRequest.getEmail() != null) {
			hotel.setEmail(hotelRequest.getEmail());
		}

		hotelRepository.save(hotel);
		wrapper.setSuccess(true);
		wrapper.setMessage("Record Updated");
		return wrapper;

	}

	public ResponseWrapper<Void> updateAll(Hotels hotelRequest) {
		List<Hotels> hotelList = hotelRepository.findAll();
		
		if(hotelList.isEmpty()) {
			throw new DataNotFoundException("Records Not Found");
		}
		
		for(int i=0; i<hotelList.size(); i++) {
			Hotels hotel = hotelList.get(i);
			if (hotelRequest.getAddress() != null) {
				hotel.setAddress(hotelRequest.getAddress());
			}
			if (hotelRequest.getName() != null) {
				hotel.setName(hotelRequest.getName());
			}
			if (hotelRequest.getPhone() != null) {
				hotel.setPhone(hotelRequest.getPhone());
			}
			if (hotelRequest.getEmail() != null) {
				hotel.setEmail(hotelRequest.getEmail());
			}
			
			hotelList.set(i, hotel);
			
		}
		
		hotelRepository.saveAll(hotelList);
		ResponseWrapper<Void> wrapper = new ResponseWrapper<>();
		wrapper.setSuccess(true);
		wrapper.setMessage("Records Updated");
		return wrapper;
	}

	public ResponseWrapper<Hotels> getSingleHotel(int id) {
		Optional<Hotels> hotelOpt = hotelRepository.findById(id);
		if(!hotelOpt.isPresent()) {
			throw new DataNotFoundException("Records Not Found");
		}
		
	
		ResponseWrapper<Hotels> wrapper = new ResponseWrapper<>();
		wrapper.setData(hotelOpt.get());
		wrapper.setSuccess(true);
		wrapper.setMessage("Success");
		return wrapper;
	}

	public ResponseWrapper<List<Hotels>> search(String query) {
		List<Hotels> hotelList = hotelRepository.findByNameStartingWith(query);
		ResponseWrapper<List<Hotels>> wrapper = new ResponseWrapper<>();
		wrapper.setSuccess(true);
		wrapper.setData(hotelList);
		return wrapper;
	}
	
	
}
