package com.app.hotels.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.app.hotels.entities.Hotels;
import com.app.hotels.exception.UnAuthorizedException;
import com.app.hotels.response.ResponseWrapper;
import com.app.hotels.services.HotelsServices;
import com.app.hotels.services.UserService;

@RestController
public class HotelController {

	@Autowired
	private HotelsServices hotelService;

	@Autowired
	UserService userService;

	@RequestMapping(value = "/addHotel", method = RequestMethod.POST)
	public ResponseWrapper<Void> addHotel(HttpServletRequest request, @RequestBody List<Hotels> hotels) {
		String username = request.getHeader("username");
		String password = request.getHeader("password");
		String role = userService.validateUser(username, password);
		if (!role.equals("ADMIN")) {
			throw new UnAuthorizedException("Invalid Credentials");
		}
		return hotelService.addHotel(hotels);
	}

	@RequestMapping(value = "/getAllHotels", method = RequestMethod.GET)
	public ResponseWrapper<List<Hotels>> showHotels(HttpServletRequest request) {
		String username = request.getHeader("username");
		String password = request.getHeader("password");
		userService.validateUser(username, password);
		return hotelService.showHotels();
	}

	@RequestMapping(value = "/searchHotel", method = RequestMethod.GET)
	public ResponseWrapper<List<Hotels>> search(HttpServletRequest request, @RequestParam(name = "q") String query) {
		String username = request.getHeader("username");
		String password = request.getHeader("password");
		userService.validateUser(username, password);
		return hotelService.search(query);
	}

	@RequestMapping(value = "/getHotel/{id}", method = RequestMethod.GET)
	public ResponseWrapper<Hotels> showSingleHotel(HttpServletRequest request,
			@PathVariable(value = "id", required = true) int id) {
		String username = request.getHeader("username");
		String password = request.getHeader("password");
		userService.validateUser(username, password);
		return hotelService.getSingleHotel(id);
	}

	@RequestMapping(value = "/deleteHotel/{id}", method = RequestMethod.DELETE)
	public ResponseWrapper<Void> deleteHotel(HttpServletRequest request,
			@PathVariable(value = "id", required = true) int id) {
		String username = request.getHeader("username");
		String password = request.getHeader("password");
		String role = userService.validateUser(username, password);
		if (!role.equals("ADMIN")) {
			throw new UnAuthorizedException("Invalid Credentials");
		}
		return hotelService.deleteHotel(id);
	}

	@RequestMapping(value = "/updateHotelData/{id}", method = RequestMethod.PUT)
	public ResponseWrapper<Void> updateDetails(HttpServletRequest request, @RequestBody Hotels ht,
			@PathVariable(value = "id", required = true) int id) {
		String username = request.getHeader("username");
		String password = request.getHeader("password");
		String role = userService.validateUser(username, password);
		if (!role.equals("ADMIN")) {
			throw new UnAuthorizedException("Invalid Credentials");
		}
		return hotelService.updateDetails(id, ht);
	}

	@RequestMapping(value = "/updateAll", method = RequestMethod.PUT)
	public ResponseWrapper<Void> updateAll(HttpServletRequest request, @RequestBody Hotels hotelReq) {
		String username = request.getHeader("username");
		String password = request.getHeader("password");
		String role = userService.validateUser(username, password);
		if (!role.equals("ADMIN")) {
			throw new UnAuthorizedException("Invalid Credentials");
		}
		return hotelService.updateAll(hotelReq);
	}
}
