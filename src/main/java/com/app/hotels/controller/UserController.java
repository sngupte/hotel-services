package com.app.hotels.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.app.hotels.entities.UserProfile;
import com.app.hotels.response.ResponseWrapper;
import com.app.hotels.services.UserService;

@RestController
public class UserController {

	@Autowired UserService userService;
	
	@RequestMapping(value = "/registerUser", method = RequestMethod.POST)
	ResponseWrapper<Void> register(@RequestBody UserProfile user){
		return userService.registerUser(user);
	}
}
