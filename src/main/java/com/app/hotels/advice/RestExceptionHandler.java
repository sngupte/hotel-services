package com.app.hotels.advice;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.app.hotels.exception.BadRequestException;
import com.app.hotels.exception.DataNotFoundException;
import com.app.hotels.exception.UnAuthorizedException;
import com.app.hotels.response.ResponseWrapper;

@Controller
@ControllerAdvice
public class RestExceptionHandler {

	@ExceptionHandler(DataNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ResponseBody
	public ResponseWrapper<Void> dataNotFound(Exception ex) {
		ResponseWrapper<Void> errorResponse = new ResponseWrapper<>();
		errorResponse.setMessage(ex.getMessage());
		errorResponse.setSuccess(false);
		return errorResponse;
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler({ MissingServletRequestParameterException.class, BadRequestException.class })
	@ResponseBody
	ResponseWrapper<Void> badRequest(Exception ex) {
		ResponseWrapper<Void> errorResponse = new ResponseWrapper<>();
		errorResponse.setMessage(ex.getMessage());
		errorResponse.setSuccess(false);
		return errorResponse;
	}
	
	@ResponseStatus(HttpStatus.UNAUTHORIZED)
	@ExceptionHandler(UnAuthorizedException.class)
	@ResponseBody
	ResponseWrapper<Void> unAuthorized(Exception ex) {
		ResponseWrapper<Void> errorResponse = new ResponseWrapper<>();
		errorResponse.setMessage(ex.getMessage());
		errorResponse.setSuccess(false);
		return errorResponse;
	}

}
