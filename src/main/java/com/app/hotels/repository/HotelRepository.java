package com.app.hotels.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.hotels.entities.Hotels;

@Repository
public interface HotelRepository extends JpaRepository<Hotels, Integer> {
	List<Hotels> findByNameStartingWith(String name);
}
