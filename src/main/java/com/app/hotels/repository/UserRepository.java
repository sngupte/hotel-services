package com.app.hotels.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.hotels.entities.UserProfile;

@Repository
public interface UserRepository extends JpaRepository<UserProfile, Integer> {
	Optional<UserProfile> findByEmailAndPassword(String email, String password);
}
